//
//  CalculatorLogic.swift
//  Calculator
//
//  Created by Parinay on 25/02/21.
//

import Foundation

struct Calculator {
    
    private var num: Double?
    
    private var midCalculation: (n1: Double, calcMethod: String)?
    
    mutating func setNumber(_ number: Double) {
        self.num = number
    }
    
    mutating func calculate(symbol: String) -> Double? {
       
        if let n = num {
            switch symbol {
            case "+/-":
                return n * -1
            case "AC":
                return 0
            case "%":
                return n * 0.01
            case "=":
                return performTwoNumCalculation(n2: n)
            default:
                midCalculation = (n1: n, calcMethod: symbol)
            }
        }
        return nil 
    }
    
    private func performTwoNumCalculation(n2: Double) -> Double? {
        
        if let n1 = midCalculation?.n1,
            let operation = midCalculation?.calcMethod {
            
            switch operation {
            case "+":
                return n1 + n2
            case "-":
                return n1 - n2
            case "×":
                return n1 * n2
            case "÷":
                return n1 / n2
            default:
                fatalError("The operation passed in does not match any of the cases.")
            }
        }
        return nil
    }
    
}
