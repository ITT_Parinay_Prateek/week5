//
//  ViewController.swift
//  Calculator
//
//  Created by Parinay on 25/02/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var calculatorDisplay: UILabel!
    
    private var isFinishedTypingNumber: Bool = true
    
    private var displayValue: Double {
        get {
            guard let number = Double(calculatorDisplay.text!) else {
                fatalError("Cannot convert display label text to a Double.")
            }
            return number
        }
        set {
            calculatorDisplay.text = String(newValue)
        }
    }
    
    private var calculator = Calculator()
    
    @IBAction func nonNumericButtonPressed(_ sender: UIButton) {
        
        //When Non-numeric key pressed
        
        isFinishedTypingNumber = true
        
        calculator.setNumber(displayValue)
        
        if let calculationSymbol = sender.currentTitle {
 
            if let result = calculator.calculate(symbol: calculationSymbol) {
                displayValue = result
            }
        }
    }

    
    @IBAction func numButtonPressed(_ sender: UIButton) {
        
        //When numeric key pressed
        
        if let numValue = sender.currentTitle {
            
            if isFinishedTypingNumber {
                calculatorDisplay.text = numValue
                isFinishedTypingNumber = false
            } else {
                
                if numValue == "." {
                    
                    let isInt = floor(displayValue) == displayValue
                    
                    if !isInt {
                        return
                    }
                }
                calculatorDisplay.text = calculatorDisplay.text! + numValue
            }
        }
    }

}

